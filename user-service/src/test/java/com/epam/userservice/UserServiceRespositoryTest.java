package com.epam.userservice;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.epam.userservice.entity.User;
import com.epam.userservice.repository.UserRespository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
class UserServiceRespositoryTest {

	@Autowired
	UserRespository userRespository;

	@Test
	void testRepository() {

		User user = new User();
		user.setUserName("Ram123");
		user.setName("RAM N");
		user.setPassword("ram12345");

		userRespository.save(user);

		assertThat(user.getUserId()).isNotNull();
	}

}
