package com.epam.userservice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.userservice.dto.UserDto;
import com.epam.userservice.entity.User;
import com.epam.userservice.restcontroller.UserResource;
import com.epam.userservice.service.UserService;

@ExtendWith(MockitoExtension.class)
class UserServiceRestControllerTest {

	@InjectMocks
	UserResource userResource;

	@Mock
	UserService userService;

	@Test
	void findAllTest() {

		User user = new User(1, "Ram123", "whale12345", "Ram N");
		List<User> allUsers = Arrays.asList(user);
		Mockito.when(userService.findAll()).thenReturn(allUsers);
		List<UserDto> result = userResource.findAll().getBody();
		assertThat(result.size()).isEqualTo(1);

		assertThat(result.get(0).getUserId()).isEqualTo(allUsers.get(0).getUserId());

	}

	@Test
	void findById() {

		User user = new User(1, "Ram123", "whale12345", "Ram N");
		Mockito.when(userService.findById(user.getUserId())).thenReturn(user);
		UserDto result = userResource.findByUserId(1).getBody();
		assertThat(result.getUserId()).isEqualTo(user.getUserId());

	}

	@Test
	void createUser() {

		User user = new User(1, "Ram123", "whale12345", "Ram N");
		UserDto userDto = new UserDto(1, "Ram123", "whale12345", "Ram N");
		lenient().when(userService.save(user)).thenReturn(user);
		String result = userResource.createUser(userDto).getBody();
		assertThat(result).isEqualTo("User Created.");

	}

	@Test
	void updateUser() {

		User user = new User(1, "Ram123", "whale12345", "Ram N");
		UserDto userDto = new UserDto(1, "Ram123", "whale12345", "Ram N");
		lenient().when(userService.updateById(user.getUserId(), user)).thenReturn(true);
		String result = userResource.updateByUserId(1, userDto).getBody();
		assertThat(result).isEqualTo("User Updated.");

	}

	@Test
	void deleteUser() {

		User user = new User(1, "Ram123", "whale12345", "Ram N");
		Mockito.when(userService.deleteById(user.getUserId())).thenReturn(true);
		String result = userResource.deleteByUserId(1).getBody();
		assertThat(result).isEqualTo("User Deleted.");

	}

}
