package com.epam.userservice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.userservice.entity.User;
import com.epam.userservice.exception.EmptyUserListException;
import com.epam.userservice.exception.UserNotFoundException;
import com.epam.userservice.repository.UserRespository;
import com.epam.userservice.service.UserService;

class UserServiceTest {

	@InjectMocks
	UserService userService;

	@Mock
	private UserRespository userRespository;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void findAll() throws EmptyUserListException {

		User user = new User(1, "Ram123", "whale12345", "Ram N");
		List<User> allUsers = Arrays.asList(user);
		Mockito.when(userRespository.findAll()).thenReturn(allUsers);
		List<User> result = userService.findAll();
		assertThat(result.size()).isEqualTo(1);
		assertThat(result.get(0).getUserId()).isEqualTo(allUsers.get(0).getUserId());
		
	}

	@Test
	void FindByUserId() throws UserNotFoundException {
		Integer userId = 1;

		User user = new User(1, "Ram123", "whale12345", "Ram N");
		Mockito.when(userRespository.findByUserId(user.getUserId())).thenReturn(user);
		User userFound = userService.findById(userId);
		assertThat(userFound.getUserId()).isEqualTo(user.getUserId());

	}

	@Test
	void saveUserTest() {

		User user = new User(1, "Ram123", "whale12345", "Ram N");
		when(userRespository.save(user)).thenReturn(user);
		User userFound = userService.save(user);
		assertThat(userFound.getUserId()).isEqualTo(user.getUserId());

	}

	@Test
	void deleteUserByIdtest() throws UserNotFoundException {
		
		User user = new User(1, "Ram123", "whale12345", "Ram N");
		when(userRespository.findByUserId(1)).thenReturn(user);
		userService.deleteById(1);
		verify(userRespository, times(1)).delete(user);

	}

	@Test
	void updateBookByIdtest() throws UserNotFoundException {

		User user = new User(1, "Ram12345", "whale12345", "Ram N");
		when(userRespository.findByUserId(1)).thenReturn(user);
		boolean ifUpdated = userService.updateById(1, user);
		assertThat(ifUpdated).isEqualTo(true);

	}

}
