package com.epam.userservice.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

/**
 * Represents a User DTO.
 * 
 * @author Vandana Priya
 * @version 1.0
 */

public class UserDto {

	/**
	 * Represents the Id of the User.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer userId;

	/**
	 * Represents the userName of the User.
	 */
	@Column(name = "user_name", unique = true)
	@Pattern(regexp = "^[a-zA-Z0-9]{3,}$", message = "Accepts only alphabets and digits. Minimum length is 3 ")
	String userName;

	/**
	 * Represents the password of the User.
	 */
	@Pattern(regexp = "^[a-zA-Z0-9]{5,}$", message = "Accepts only alphabets and digits. Minimum length is 5 ")
	String password;

	/**
	 * Represents the Name of the User.
	 */
	@Pattern(regexp = "^[a-zA-Z ]{3,}$", message = "Accepts only alphabets. Minimum length is 3 ")
	String name;

	public UserDto() {
		super();
	}

	/**
	 * Creates a user with the specified values.
	 * 
	 * @param userId   The user’s Id.
	 * @param userName The user’s userName.
	 * @param password The user’s password.
	 * @param name     The user’s name.
	 */

	public UserDto(Integer userId,
			@Pattern(regexp = "^[a-zA-Z0-9]{3,}$", message = "Accepts only alphabets and digits. Minimum length is 3 ") String userName,
			@Pattern(regexp = "^[a-zA-Z0-9]{5,}$", message = "Accepts only alphabets and digits. Minimum length is 5 ") String password,
			@Pattern(regexp = "^[a-zA-Z ]{3,}$", message = "Accepts only alphabets. Minimum length is 3 ") String name) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.name = name;
	}

	/**
	 * Gets the user’s Id.
	 * 
	 * @return An Integer representing the user Id.
	 */

	public Integer getUserId() {
		return userId;
	}

	/**
	 * Sets the user’s Id.
	 * 
	 * @param userId An Integer containing the user Id.
	 */

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user’s userName.
	 * 
	 * @return A String representing the user’s userName.
	 */

	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user’s userName.
	 * 
	 * @param userName A String containing the user’s userName.
	 */

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the user’s password.
	 * 
	 * @return A String representing the user’s password.
	 */

	public String getPassword() {
		return password;
	}

	/**
	 * Sets the user’s password.
	 * 
	 * @param password A String containing the user’s password.
	 */

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the user’s name.
	 * 
	 * @return A String representing the user’s name.
	 */

	public String getName() {
		return name;
	}

	/**
	 * Sets the user’s name.
	 * 
	 * @param name A String containing the user’s name.
	 */

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the User Information.
	 * 
	 * @return A String representing the user Information.
	 */

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", password=" + password + ", name=" + name + "]";
	}

}
