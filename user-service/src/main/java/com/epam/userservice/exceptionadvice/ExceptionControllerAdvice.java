package com.epam.userservice.exceptionadvice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.userservice.exception.EmptyUserListException;
import com.epam.userservice.exception.UserNotFoundException;

@ControllerAdvice
public class ExceptionControllerAdvice {

	@ExceptionHandler(EmptyUserListException.class)
	public ResponseEntity<Object> handleEmptyUserListException(RuntimeException exception) {

		String bodyOfResponse = "EXCEPTION OCCURED:";
		return new ResponseEntity<>(bodyOfResponse + exception.getMessage(), HttpStatus.NO_CONTENT);

	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<Object> handleUserNotFoundException(RuntimeException exception) {

		String bodyOfResponse = "EXCEPTION OCCURED:";
		return new ResponseEntity<>(bodyOfResponse + exception.getMessage(), HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleException(Exception exception) {

		String bodyOfResponse = "EXCEPTION OCCURED:";
		return new ResponseEntity<>(bodyOfResponse + exception.getMessage(), HttpStatus.CONFLICT);

	}

}
