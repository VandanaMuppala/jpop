package com.epam.userservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.userservice.entity.User;
import com.epam.userservice.exception.EmptyUserListException;
import com.epam.userservice.exception.UserNotFoundException;
import com.epam.userservice.repository.UserRespository;

/**
 * Represents a Services to perform on Book.
 * 
 * @author Vandana Priya
 * @version 1.0
 */

@Service
public class UserService {

	@Autowired
	UserRespository userRespository;

	/**
	 * Gets the list of all users in repository.
	 * 
	 * @throws EmptyUserListException if list is empty.
	 * @return A list of users present in the repository.
	 */

	public List<User> findAll() throws EmptyUserListException {

		List<User> userList = userRespository.findAll();
		if (userList.isEmpty()) {
			throw new EmptyUserListException();
		}

		return userList;
	}

	/**
	 * Gets the user of particular userId in repository.
	 * 
	 * @param userId An Integer containing id of user to find in repository.
	 * @throws UserNotFoundException if user is not found.
	 * @return An user present in the repository.
	 */

	public User findById(Integer userId) throws UserNotFoundException {

		User user = userRespository.findByUserId(userId);
		if (user == null) {
			throw new UserNotFoundException(userId);
		}
		return user;

	}

	/**
	 * Gets the user of saved user in repository.
	 * 
	 * @param user A user containing user to add to repository.
	 * @return An user present in the repository.
	 */

	public User save(User user) {

		return userRespository.save(user);

	}

	/**
	 * The method deletes user information in repository based on user id.
	 * 
	 * @param userId An Integer containing id of user to delete in repository.
	 * @throws UserNotFoundException if user is not found.
	 * @return A boolean value.
	 */

	public boolean deleteById(Integer userId) throws UserNotFoundException {

		userRespository.delete(findById(userId));
		return true;

	}

	/**
	 * The method updates user information in repository based on user id.
	 * 
	 * @param userId An Integer containing id of user to update in repository.
	 * @param user   An user containing user to update to repository.
	 * @throws UserNotFoundException if user is not found.
	 * @return A boolean value.
	 */

	public boolean updateById(Integer userId, User user) throws UserNotFoundException {

		User userToUpdate = userRespository.findByUserId(userId);
		if (userToUpdate == null) {
			throw new UserNotFoundException(userId);
		}

		userToUpdate.setUserName(user.getUserName());
		userToUpdate.setPassword(user.getPassword());
		userToUpdate.setName(user.getName());

		userRespository.save(userToUpdate);

		return true;
	}

}
