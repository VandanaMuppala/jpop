package com.epam.userservice.restcontroller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.userservice.dto.UserDto;
import com.epam.userservice.entity.User;
import com.epam.userservice.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Represents a User API.
 * 
 * @author Vandana Priya
 * @version 1.0
 */

@RestController
@RequestMapping(value = "users")
@Api(value = "This is a simple user managment system. We can access, add, update, delete a particular user")
public class UserResource {

	@Autowired
	UserService userService;

	ModelMapper modelMapper = new ModelMapper();

	@GetMapping
	@ApiOperation(value = "Find all users.", response = Page.class)
	public ResponseEntity<List<UserDto>> findAll() {

		List<UserDto> userDtoList = userService.findAll().stream().map(this::convertToDto).collect(Collectors.toList());
		return new ResponseEntity<>(userDtoList, HttpStatus.OK);

	}

	@GetMapping("/{user_id}")
	@ApiOperation(value = "Find user by userId. ", response = Page.class)
	public ResponseEntity<UserDto> findByUserId(
			@ApiParam(value = "Id of user to return", required = true) @Valid @PathVariable("user_id") Integer userId) {

		UserDto userDto = convertToDto(userService.findById(userId));
		return new ResponseEntity<>(userDto, HttpStatus.OK);

	}

	@PostMapping
	@ApiOperation(value = "Add a User. ", response = User.class)
	public ResponseEntity<String> createUser(
			@ApiParam(value = "A User object that needs to be added.", required = true) @Valid @RequestBody UserDto userDto) {

		User user = convertToEntity(userDto);
		userService.save(user);
		return new ResponseEntity<>("User Created.", HttpStatus.OK);

	}

	@DeleteMapping("{user_id}")
	@ApiOperation(value = "Delete a User")
	public ResponseEntity<String> deleteByUserId(
			@ApiParam(value = "UserId of User to be deleted.", required = true) @Valid @PathVariable("user_id") Integer userId) {

		userService.deleteById(userId);
		return new ResponseEntity<>("User Deleted.", HttpStatus.OK);

	}

	@PutMapping("{user_id}")
	@ApiOperation(value = "Update a User")
	public ResponseEntity<String> updateByUserId(
			@ApiParam(value = "UserId of User to be updated.", required = true) @PathVariable("user_id") Integer userId,
			@ApiParam(value = "A User object that needs to be updated.", required = true) @Valid @RequestBody UserDto userDto) {
		
		User user = convertToEntity(userDto);
		userService.updateById(userId, user);
		return new ResponseEntity<>("User Updated.", HttpStatus.OK);

	}

	private UserDto convertToDto(User user) {
		return modelMapper.map(user, UserDto.class);

	}

	private User convertToEntity(UserDto userDto) {
		return modelMapper.map(userDto, User.class);

	}

}
