package com.epam.userservice.exception;

/**
 * Represents a EmptyUserListException custom exception.
 * 
 * @author Vandana Priya
 * @version 1.0
 */

public class EmptyUserListException extends RuntimeException {
	
	/**
	 * Gets the Exception Information.
	 * 
	 * @return A String representing the information of exception.
	 */
	@Override
	public String getMessage() {

		return "No User found.";

	}

}
