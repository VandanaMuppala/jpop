package com.epam.userservice.exception;

/**
 * Represents a UserNotFoundException custom exception.
 * 
 * @author Vandana Priya
 * @version 1.0
 */

public class UserNotFoundException extends RuntimeException {

	/**
	 * Represents the Id of the user not found.
	 */
	Integer userId = null;

	/**
	 * Assigns value to Id of user not found with the specified value.
	 * 
	 * @param userId The user's Id.
	 */
	public UserNotFoundException(Integer userId) {

		this.userId = userId;

	}

	/**
	 * Gets the Exception Information.
	 * 
	 * @return A String representing the information of exception.
	 */

	@Override
	public String getMessage() {
		return " User Not Found: " + userId;
	}

}
