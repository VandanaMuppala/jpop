package com.epam.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.userservice.entity.User;

@Repository
public interface UserRespository extends JpaRepository<User, String> {

	User findByUserId(Integer userId);

}
