package com.epam.bookservice;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.epam.bookservice.entity.Book;
import com.epam.bookservice.repository.BookRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
class BookServiceRepositoryTest {

	@Autowired
	BookRepository bookRepository;

	@Test
	void testRepository() {
		
		Book book = new Book();
		book.setTitle("Harry Potter");
		book.setAuthor("J K Rowling");
		book.setCategory("Fantasy");
		book.setDescription("Boy exploring magical world");
		
		bookRepository.save(book);
		
		assertThat(book.getBookId()).isNotNull();

	}

}
