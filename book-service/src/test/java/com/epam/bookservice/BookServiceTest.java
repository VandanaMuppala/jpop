package com.epam.bookservice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.bookservice.entity.Book;
import com.epam.bookservice.exception.BookNotFoundException;
import com.epam.bookservice.exception.EmptyBookListException;
import com.epam.bookservice.repository.BookRepository;
import com.epam.bookservice.service.BookService;

class BookServiceTest {

	@InjectMocks
	BookService bookService;

	@Mock
	private BookRepository bookRepository;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void findAll() throws EmptyBookListException {

		Book book = new Book(1, "ABC", "John", "THRILLER", "It is a thriller.");
		List<Book> allBooks = Arrays.asList(book);
		Mockito.when(bookRepository.findAll()).thenReturn(allBooks);
		List<Book> result = bookService.findAll();
		assertThat(result.size()).isEqualTo(1);
		assertThat(result.get(0).getBookId()).isEqualTo(allBooks.get(0).getBookId());

	}

	@Test
	void findByBookId() throws BookNotFoundException {
		Integer bookId = 1;

		Book book = new Book(1, "ABC", "John", "THRILLER", "It is a thriller.");
		Mockito.when(bookRepository.findByBookId(book.getBookId())).thenReturn(book);
		Book bookFound = bookService.findById(bookId);
		assertThat(bookFound.getBookId()).isEqualTo(book.getBookId());

	}

	@Test
	void saveBookTest() {

		Book book = new Book(1, "ABC", "John", "THRILLER", "It is a thriller.");
		when(bookRepository.save(book)).thenReturn(book);
		Book bookFound = bookService.save(book);
		assertThat(bookFound.getBookId()).isEqualTo(book.getBookId());

	}

	@Test
	void deleteBookByIdtest() throws BookNotFoundException {
		
		Book book = new Book(1, "ABC", "John", "THRILLER", "It is a thriller.");
		Mockito.when(bookRepository.findByBookId(book.getBookId())).thenReturn(book);
		bookService.deleteById(book.getBookId());
		verify(bookRepository, times(1)).delete(book);

	}

	@Test
	void updateBookByIdtest() throws BookNotFoundException {

		Book book = new Book(1, "ABC", "John", "THRILLER", "It is a thriller.");
		Mockito.when(bookRepository.findByBookId(book.getBookId())).thenReturn(book);
		boolean ifUpdated = bookService.updateById(1, book);
		assertThat(ifUpdated).isEqualTo(true);

	}

}
