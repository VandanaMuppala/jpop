package com.epam.bookservice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.bookservice.dto.BookDto;
import com.epam.bookservice.entity.Book;
import com.epam.bookservice.restcontroller.BookResource;
import com.epam.bookservice.service.BookService;

@ExtendWith(MockitoExtension.class)
class BookServiceRestControllerTest {

	@InjectMocks
	BookResource bookResource;

	@Mock
	BookService bookService;
	
	@Test
	void findAllTest() {

		Book book = new Book(1, "ABC", "John", "THRILLER", "It is a thriller.");
		List<Book> allBooks = Arrays.asList(book);
		Mockito.when(bookService.findAll()).thenReturn(allBooks);
		List<BookDto> result = bookResource.findAll().getBody();
		assertThat(result.size()).isEqualTo(1);

		assertThat(result.get(0).getBookId()).isEqualTo(allBooks.get(0).getBookId());

	}

	@Test
	void findById() {

		Book book = new Book(1, "ABC", "John", "THRILLER", "It is a thriller.");
		Mockito.when(bookService.findById(book.getBookId())).thenReturn(book);
		BookDto result = bookResource.findByBookId(1).getBody();
		assertThat(result.getBookId()).isEqualTo(book.getBookId());

	}

	@Test
	void createBook() {

		Book book = new Book(1, "ABC", "John", "THRILLER", "It is a thriller.");
		BookDto bookDto = new BookDto(1, "ABC", "John", "THRILLER", "It is a thriller.");
		lenient().when(bookService.save(book)).thenReturn(book);
		String result = bookResource.createBook(bookDto).getBody();
		assertThat(result).isEqualTo("Book Created.");

	}

	@Test
	void updateBook() {

		Book book = new Book(1, "ABC", "John", "THRILLER", "It is a thriller.");
		BookDto bookDto = new BookDto(1, "ABC", "John", "THRILLER", "It is a thriller.");
		lenient().when(bookService.updateById(book.getBookId(), book)).thenReturn(true);
		String result = bookResource.updateByBookId(1, bookDto).getBody();
		assertThat(result).isEqualTo("Book Updated.");

	}

	@Test
	void deleteBook() {

		Book book = new Book(1, "ABC", "John", "THRILLER", "It is a thriller.");
		Mockito.when(bookService.deleteById(book.getBookId())).thenReturn(true);
		String result = bookResource.deleteByBookId(1).getBody();
		assertThat(result).isEqualTo("Book Deleted.");

	}

}
