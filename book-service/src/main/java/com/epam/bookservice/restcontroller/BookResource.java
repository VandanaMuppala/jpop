package com.epam.bookservice.restcontroller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.bookservice.dto.BookDto;
import com.epam.bookservice.entity.Book;
import com.epam.bookservice.exceptionadvice.ExceptionControllerAdvice;
import com.epam.bookservice.service.BookService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Represents a Book API.
 * 
 * @author Vandana Priya
 * @version 1.0
 */

@RestController
@RequestMapping(value = "books")
@Api(value = "This is a simple book managment system. We can access, add, update, delete a particular book")
@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added a new book"),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionControllerAdvice.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionControllerAdvice.class),
		@ApiResponse(code = 404, message = "The resource you are trying to reach is not found.", response = ExceptionControllerAdvice.class),
		@ApiResponse(code = 500, message = "Internal Server Error", response = ExceptionControllerAdvice.class) })

public class BookResource {

	@Autowired
	BookService bookService;

	ModelMapper modelMapper = new ModelMapper();

	@GetMapping
	@ApiOperation(value = "Find all books.", response = Page.class)
	public ResponseEntity<List<BookDto>> findAll() {

		List<BookDto> bookDtoList = bookService.findAll().stream().map(this::convertToDto).collect(Collectors.toList());
		return new ResponseEntity<>(bookDtoList, HttpStatus.OK);

	}

	@GetMapping("/{book_id}")
	@ApiOperation(value = "Find book by bookId. ", response = Page.class)
	public ResponseEntity<BookDto> findByBookId(
			@ApiParam(value = "Id of book to return", required = true) @PathVariable("book_id") Integer bookId) {

		BookDto bookDto = convertToDto(bookService.findById(bookId));
		return new ResponseEntity<>(bookDto, HttpStatus.OK);

	}

	@PostMapping
	@ApiOperation(value = "Add a book. ", response = Book.class)
	public ResponseEntity<String> createBook(
			@ApiParam(value = "A book object that needs to be added.", required = true) @Valid @RequestBody BookDto bookDto) {
		
		Book book = convertToEntity(bookDto);
		bookService.save(book);
		return new ResponseEntity<>("Book Created.", HttpStatus.OK);

	}

	@DeleteMapping("{book_id}")
	@ApiOperation(value = "Delete a book")
	public ResponseEntity<String> deleteByBookId(
			@ApiParam(value = "BookId of Book to be deleted.", required = true) @PathVariable("book_id") Integer bookId) {

		bookService.deleteById(bookId);
		return new ResponseEntity<>("Book Deleted.", HttpStatus.OK);

	}

	@PutMapping("{book_id}")
	@ApiOperation(value = "Update a book")
	public ResponseEntity<String> updateByBookId(
			@ApiParam(value = "BookId of Book to be updated.", required = true) @PathVariable("book_id") Integer bookId,
			@ApiParam(value = "A book object that needs to be updated.", required = true) @Valid @RequestBody BookDto bookDto) {
		
		Book book = convertToEntity(bookDto);
		bookService.updateById(bookId, book);
		return new ResponseEntity<>("Book Updated.", HttpStatus.OK);

	}

	private BookDto convertToDto(Book book) {
		return modelMapper.map(book, BookDto.class);

	}

	private Book convertToEntity(BookDto bookDto) {
		return modelMapper.map(bookDto, Book.class);

	}

}
