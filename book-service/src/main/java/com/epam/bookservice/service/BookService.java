package com.epam.bookservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.bookservice.entity.Book;
import com.epam.bookservice.exception.BookNotFoundException;
import com.epam.bookservice.exception.EmptyBookListException;
import com.epam.bookservice.repository.BookRepository;

/**
 * Represents a Services to perform on Book.
 * 
 * @author Vandana Priya
 * @version 1.0
 */

@Service
public class BookService {

	@Autowired
	BookRepository bookRepository;

	/**
	 * Gets the list of all books in repository.
	 * 
	 * @throws EmptyBookListException if list is empty.
	 * @return A list of books present in the repository.
	 */

	public List<Book> findAll() throws EmptyBookListException {

		List<Book> bookList = bookRepository.findAll();

		if (bookList.isEmpty()) {
			throw new EmptyBookListException();
		}
		return bookList;
	}

	/**
	 * Gets the book of particular bookId in repository.
	 * 
	 * @param bookId An Integer containing id of book to find in repository.
	 * @throws BookNotFoundException if book is not found.
	 * @return A Book present in the repository.
	 */

	public Book findById(Integer bookId) throws BookNotFoundException {

		Book book = bookRepository.findByBookId(bookId);

		if (book == null) {
			throw new BookNotFoundException(bookId);
		}

		return book;
	}

	/**
	 * Gets the book of saved book in repository.
	 * 
	 * @param book A book containing book to add to repository.
	 * @return A Book present in the repository.
	 */

	public Book save(Book book) {

		return bookRepository.save(book);

	}

	/**
	 * The method deletes book information in repository based on book id.
	 * 
	 * @param bookId An Integer containing id of book to delete in repository.
	 * @throws BookNotFoundException if book is not found.
	 * @return A boolean value.
	 */

	public boolean deleteById(Integer bookId) throws BookNotFoundException {

		bookRepository.delete(findById(bookId));
		return true;

	}

	/**
	 * The method updates book information in repository based on book id.
	 * 
	 * @param bookId An Integer containing id of book to update in repository.
	 * @param book   A book containing book to update to repository.
	 * @throws BookNotFoundException if book is not found.
	 * @return A boolean value.
	 */

	public boolean updateById(Integer bookId, Book book) throws BookNotFoundException {

		Book bookToUpdate = bookRepository.findByBookId(bookId);

		if (bookToUpdate == null) {
			throw new BookNotFoundException(bookId);
		}

		bookToUpdate.setAuthor(book.getAuthor());
		bookToUpdate.setCategory(book.getCategory());
		bookToUpdate.setDescription(book.getDescription());

		bookRepository.save(bookToUpdate);

		return true;

	}

}
