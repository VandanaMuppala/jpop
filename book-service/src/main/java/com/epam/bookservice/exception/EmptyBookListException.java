package com.epam.bookservice.exception;

/**
 * Represents a EmptyBookListException custom exception.
 * 
 * @author Vandana Priya
 * @version 1.0
 */
public class EmptyBookListException extends RuntimeException {

	/**
	 * Gets the Exception Information.
	 * 
	 * @return A String representing the information of exception.
	 */

	@Override
	public String getMessage() {
		return "No Book Found.";
	}

}
