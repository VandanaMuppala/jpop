package com.epam.bookservice.exception;

/**
 * Represents a BookNotFoundException custom exception.
 * 
 * @author Vandana Priya
 * @version 1.0
 */

public class BookNotFoundException extends RuntimeException {

	/**
	 * Represents the Id of the book not found.
	 */
	Integer bookId = 0;

	/**
	 * Assigns value to Id of book not found with the specified value.
	 * 
	 * @param bookId The book's Id.
	 */
	public BookNotFoundException(Integer bookId) {

		this.bookId = bookId;
	}
	
	/**
	 * Gets the Exception Information.
	 * 
	 * @return A String representing the information of exception.
	 */

	@Override
	public String getMessage() {
		return " Book Not Found: " + bookId;
	}

}
