package com.epam.bookservice.exceptionadvice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.bookservice.exception.BookNotFoundException;
import com.epam.bookservice.exception.EmptyBookListException;

@ControllerAdvice
public class ExceptionControllerAdvice {

	@ExceptionHandler(EmptyBookListException.class)
	public ResponseEntity<Object> handleEmptyBookListException(RuntimeException exception) {

		String bodyOfResponse = "EXCEPTION OCCURED:";
		return new ResponseEntity<>(bodyOfResponse + exception.getMessage(), HttpStatus.NO_CONTENT);

	}

	@ExceptionHandler(BookNotFoundException.class)
	public ResponseEntity<Object> handleBookNotFoundException(RuntimeException exception) {

		String bodyOfResponse = "EXCEPTION OCCURED:";
		return new ResponseEntity<>(bodyOfResponse + exception.getMessage(), HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleException(Exception exception) {

		String bodyOfResponse = "EXCEPTION OCCURED:";
		return new ResponseEntity<>(bodyOfResponse + exception.getMessage(), HttpStatus.CONFLICT);

	}

}
