package com.epam.bookservice.dto;

public class ErrorDto {
	
	String exception;

	String message;

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ErrorDto [exception=" + exception + ", message=" + message + "]";
	}
	

}
