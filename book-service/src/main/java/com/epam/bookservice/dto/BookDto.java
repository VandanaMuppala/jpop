package com.epam.bookservice.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * Represents a Book DTO.
 * 
 * @author Vandana Priya
 * @version 1.0
 */

public class BookDto {

	/**
	 * Represents the Id of the book.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer bookId;

	/**
	 * Represents the title of the book.
	 */
	@Column(nullable = false)
	@Pattern(regexp = "^[a-zA-Z][a-zA-Z ]*$")
	String title;

	/**
	 * Represents the author of the book.
	 */
	@NotNull
	@Pattern(regexp = "^[a-zA-Z][a-zA-Z ]*$")
	String author;

	/**
	 * Represents the category the book belongs.
	 */
	@NotNull
	@Pattern(regexp = "^[a-zA-Z][a-zA-Z ]*$")
	String category;

	/**
	 * Represents a small description of the book.
	 */
	@NotNull
	@Pattern(regexp = "^[a-zA-Z][a-zA-Z 0-9]*$")
	String description;

	public BookDto() {

	}

	/**
	 * Creates a book with the specified values.
	 * 
	 * @param bookId      The book’s Id.
	 * @param title       The book’s title.
	 * @param author      The book's author.
	 * @param category    The book's category.
	 * @param description The book's description.
	 */

	public BookDto(Integer bookId, String title, String author, String category, String description) {

		this.bookId = bookId;
		this.title = title;
		this.author = author;
		this.category = category;
		this.description = description;

	}

	/**
	 * Gets the book's Id.
	 * 
	 * @return An Integer representing the book Id.
	 */

	public Integer getBookId() {
		return bookId;
	}

	/**
	 * Sets the book's Id.
	 * 
	 * @param bookId An Integer containing the book Id.
	 */

	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	/**
	 * Gets the book's title.
	 * 
	 * @return A String representing the book title.
	 */

	public String getTitle() {
		return title;
	}

	/**
	 * Sets the book's title.
	 * 
	 * @param title An String containing the book title.
	 */

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the book's author.
	 * 
	 * @return A String representing the book author.
	 */

	public String getAuthor() {
		return author;
	}

	/**
	 * Sets the book's author.
	 * 
	 * @param author A String containing the book author.
	 */

	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * Gets the book's category.
	 * 
	 * @return A String representing the book category.
	 */

	public String getCategory() {
		return category;
	}

	/**
	 * Sets the book's category.
	 * 
	 * @param category A String containing the book category.
	 */

	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * Gets the book's description.
	 * 
	 * @return A String representing the book description.
	 */

	public String getDescription() {
		return description;
	}

	/**
	 * Sets the book's description.
	 * 
	 * @param description A String containing the book description.
	 */

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the book Information.
	 * 
	 * @return A String representing the book Information.
	 */

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", title=" + title + ", author=" + author + ", category=" + category
				+ ", description=" + description + "]";
	}

}
